//Autor: Kamil Zieli�ski
//email: kamil.zielinski.1992@gmail.com
//wykonano 20.07.2014
//poprawki 23.05.2016

#include <iostream>
#include <fstream>
#include <sstream>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <time.h>
using namespace std;

//Menu`s function declaration
void main_menu(void);
void game_menu(int **tab, int size_of_square);
void about(void);
//Games`s function declaration
void create_map(void);
void print_map(int** tab, int n);
void tab_initialization(int** tab, int n);
void taking_arguments(int** tab, int n);
int my_pow(int number);
bool check_vertical(int **tab, int x, int y, int size_of_square, int value);
bool check_horizontal(int **tab, int x, int y, int size_of_square, int value);
bool check_square(int **tab, int x, int y, int size_of_square, int value);

int main()
{
    main_menu();

    return 0;
}
/**********************************
*** Menu Function`s Definition ***
**********************************/
void main_menu(void){

    int option=0;
    do{
        cout << "Menu: " << endl;
        cout << "\t 1. Sudoku " << endl << "\t 2. About"<< endl << "\t 3. Quit"<< endl;
        cin >> option;

        switch(option)
        {
            case 1:
            {
                system("cls");
                create_map();
                break;
            }
            case 2:
            {
                system("cls");
                about();
                break;
            }
            case 3:
            {
                system("cls");
                cout<<endl<<"\t****Option 3 [Quit]"<<endl;
                break;
            }
            default:
            {
                system("cls");
                cout<<"\tError, try again!"<<endl;
            }
        }
    }while(option!=3);
}
//game`s menu
void game_menu(int **tab, int size_of_square){

    int option=0, size_of_map=my_pow(size_of_square);
    do{
        cout << "\t 1. Podaj wspolrzedna " << endl << "\t 2. Drukuj tablice"<< endl <<"\t 3. Quit"<< endl;
        cin >> option;

        switch(option)
        {
            case 1:
            {
                /** teraz user podaje wartosci **/
                taking_arguments(tab, size_of_square);
                break;
            }
            case 2:
            {
                print_map(tab, size_of_map);
                cout<< "Fine, press enter.." << endl;
                getch(); //function for conio.h, just wait for press button, I recommend 'enter' !!
                system("cls");
               break;
            }
            case 3:
            {
                system("cls");
                cout<<endl<<"\t****Option 3 [Quit]"<<endl;
                break;
            }
            default:
            {
                cout<<"\tError, try again!"<<endl;
                system("cls");
                break;
            }
            break;
        }
        break;
    }while(option!=3);
}
//about
void about(void){
    cout << "Sudoku"<<endl<<"Autor: Kamil Zielinski"<<endl;
    cout<<"email: kamil.zielinski.1992@gmail.com"<<endl<<"wykonano 20.07.2014"<<endl<<endl;
    cout<<"Info: Podaj rozmiar mapy 2 -3"<<endl;
    cout<<endl<< "Fine, press enter.." << endl;
    getch(); //function for conio.h, just wait for press button, I recommend 'enter' !!
    system("cls");
    main_menu();
}
/**********************************
*** Games`s function Definition ***
**********************************/
void create_map(void){

/*** Podaj rozmiar kwadracika***/
    int size_of_square=0; //rozmiar planszy
    do{
        cout<<"Give a size [2-3]: " << endl;
        cin>>size_of_square;
        if(!(size_of_square>=2 && size_of_square<=3)) cout << "Error, one more time!" <<endl;
        else break;
/**Wczytuj poki rozmiar bedzie [2-5] **/
    }while(true);

/**Rozmiar calej planszy == rozmiar kwadracika^2 **/
    int size_of_map = my_pow(size_of_square);

/** utworz tablice tab */
    int **tab = new int * [size_of_map];
// generowanie poszczegolnych wymiar�w
    for (int i = 0; i<size_of_map; i++)
        tab[i] = new int [size_of_map];

/** zainicjalizuj zerami */
    tab_initialization(tab, size_of_map);
//drukowanie kontrolne
    print_map(tab, my_pow(size_of_square));

/** teraz user podaje**/
    game_menu(tab, size_of_square);

    // zwalnianie pamieci
    for (int i = 0; i<size_of_map; i++)
        delete [] tab[i];

    delete [] tab;
}

/*********** Drukowanie **********
**********************************/
void print_map(int** tab, int n){ //tablica i size_of_map czyli rozmiar planszy
    cout<< "_\t";
    for(int i=0;i<n;i++){
		cout<<i<<"\t";
	}
    cout<<endl;
	for(int i=0;i<n;i++){
		cout<<i<<"\t";
		for(int j=0;j<n;j++){
            cout << tab[i][j]<<"\t";
		}
		cout<<endl;
	}
	cout<<endl;
}

/****** Inicjalizacja zerami ******
**********************************/
void tab_initialization(int** tab, int n){
    for(int i=0;i<n;i++){
        for(int j=0;j<n;j++){
            tab[i][j]=0;
        }
    }
}

/**** reczne uzupelnianie *******
**********************************/
void taking_arguments(int** tab, int size_of_square){
    int x, y, value;
    int size_of_map=my_pow(size_of_square);

    bool is_ok = false;
    do{
        //wczytywanie wspolrzednej x
        do{
            cout<<"Podaj wspolrzedna x : [0-rozmiar planszy]" <<endl;
            cin >> x;
            if(! (x>=0 && x<size_of_map)){
                cout << "Zle podana wspolrzedna, jeszcze raz!..." << endl;
             }
            else break;
        }while(true);
        //wczytywanie wspolrzednej y
        do{
            cout<<"Podaj wspolrzedna y : [0-rozmiar planszy]" <<endl;
            cin >> y;
            if(! (y>=0 && y<size_of_map)){
                cout << "Zle podana wspolrzedna, jeszcze raz!..." << endl;
             }
            else break;
        }while(true);
        //wczytywanie wartosci do wpisania we wskazane wspolrzedne
        do{
            cout<<"Podaj wartosc tej komorki: [1-rozmiar planszy]" <<endl;
            cin >> value;
            if(! (value>0 && value<=size_of_map)){
                cout << "Zle podana wspolrzedna, jeszcze raz!..." << endl;
             }
            else break;
        }while(true);

        if( check_vertical(tab, x, y, size_of_square, value)){
            if( check_horizontal(tab, x, y, size_of_square, value) ){
                if (check_square(tab, x, y, size_of_square, value)){
                    cout<<"Wszystko gra, wpisuje..."<<endl;
                    tab[y][x] = value;
                    is_ok = true;
                }else{
                    cout<<"!!! Kwadrat zly, podaj jeszcze raz !!!"<<endl;
                }
            }else{
                cout<<"!!! Poziom zly, podaj jeszcze raz !!!"<<endl;
            }
        }else{
            cout<<"!!! Pion zly, podaj jeszcze raz !!!"<<endl;
        }
        print_map(tab, my_pow(size_of_square));

    }while(!is_ok); //dopoki nie jest ok, petla sie wykonuje

    cout<< "Fine, press enter.." << endl;
        getch(); //function for conio.h, just wait for press button, I recommend 'enter' !!
        system("cls");
        game_menu(tab, size_of_square);
}

//my version of pow() function
int my_pow(int number){
    return number*number;
}

//check values
bool check_vertical(int **tab, int x, int y, int size_of_square, int value){
    bool is_ok = true;
    int size_of_map = my_pow(size_of_square);

    for(int i=0; i<size_of_map; i++){
        if(tab[i][x] == value) {
            is_ok = false;
            cout << "!!! Blad, powtorzenie w pionie!!!"<< endl;
            break;
        }
    }
    return is_ok;
}

bool check_horizontal(int **tab, int x, int y, int size_of_square, int value){
    bool is_ok=true;
    int size_of_map = my_pow(size_of_square);

    for(int i=0; i<size_of_map; i++){
        if(tab[y][i] == value) {
            is_ok = false;
            cout << "!!! Blad, powtorzenie w poziomie!!!"<< endl;
            break;
        }
    }
    return is_ok;
}

bool check_square(int **tab, int x, int y, int size_of_square, int value){
    bool is_ok = true;
    short int kwadrat_x= (x/size_of_square)+1;
    short int kwadrat_y= (y/size_of_square)+1;

    for(int i=((kwadrat_x-1)*size_of_square); i<(size_of_square*kwadrat_x); i++){
        for(int j=((kwadrat_y-1)*size_of_square); j<(size_of_square*kwadrat_y); j++){
            if(tab[j][i] == value) {
                cout <<endl << "!!! Blad, powtorzenie w kwadracie !!!"<< endl;
                is_ok = false;
                break;
            }
        }
    }
    return is_ok;
}
